FROM gradle:8.2-jdk17 AS build
COPY --chown=gradle:gradle . /home/gradle/src
WORKDIR /home/gradle/src
RUN gradle build --no-daemon

FROM openjdk:17-jdk

RUN mkdir /app

COPY --from=build /home/gradle/src/build/libs/ /app/

ARG NAME=MSS
ARG WIDTH=6000
ARG HEIGHT=6000
ARG ROW_SIZE=6
ARG COLUMN_SIZE=6
ARG PORT=8080
ARG LOGSTASH_PORT=5000
ARG LOG_TYPE=1
ARG LOG_FILE=logfile
ARG LOG_MODE=logstash

ENV NAME=${NAME} \
    WIDTH=${WIDTH} \
    HEIGHT=${HEIGHT} \
    ROW_SIZE=${ROW_SIZE} \
    COLUMN_SIZE=${COLUMN_SIZE} \
    PORT=${PORT} \
    LOGSTASH_PORT=${LOGSTASH_PORT} \
    LOG_TYPE=${LOG_TYPE} \
    LOG_FILE=${LOG_FILE} \
    LOG_MODE=${LOG_MODE}

ENTRYPOINT java \
    -DLOGSTASH_PORT=${LOGSTASH_PORT} \
    -DLOG_FILE=${LOG_FILE} \
    -DLOG_MODE=${LOG_MODE} \
    -jar /app/MandelbrotSetServer-standalone.jar \
    -name ${NAME} \
    -width ${WIDTH} -height ${HEIGHT} \
    -rowSize ${ROW_SIZE} -columnSize ${COLUMN_SIZE} \
    -port ${PORT} \
    -logType ${LOG_TYPE}

## docker build -t mandelbrotset-server .