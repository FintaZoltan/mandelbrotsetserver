import mandelbrot.Complex
import mandelbrot.MandelbrotSet
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import util.Size

class MandelbrotSetTests {
    private val absoluteTolerance = 1.0E-14

    @Test
    fun testMapPixelToComplex() {
        val mandelbrotSet = MandelbrotSet(
            from = Complex(-2.0, -1.5),
            to = Complex(1.0, 1.5),
            imageSize = Size(1000, 1000)
        )

        val firstPixel = mandelbrotSet.mapPixelToComplex(0, 0)
        val middlePixel = mandelbrotSet.mapPixelToComplex(500, 500)
        val lastPixel = mandelbrotSet.mapPixelToComplex(1000, 1000)

        assertEquals(-2.0, firstPixel.real, absoluteTolerance)
        assertEquals(-1.5, firstPixel.imaginary, absoluteTolerance)

        assertEquals(-0.5, middlePixel.real, absoluteTolerance)
        assertEquals(0.0, middlePixel.imaginary, absoluteTolerance)

        assertEquals(1.0, lastPixel.real, absoluteTolerance)
        assertEquals(1.5, lastPixel.imaginary, absoluteTolerance)
    }

    @Test
    fun testCalculatePoint() {
        val mandelbrotSet = MandelbrotSet(
            from = Complex(-2.0, -1.5),
            to = Complex(1.0, 1.5),
            imageSize = Size(1000, 1000)
        )

        assertEquals(0, mandelbrotSet.calculatePoint(Complex(-2.0, 1.5), 200).iterationCount)
        assertEquals(1, mandelbrotSet.calculatePoint(Complex(-1.099615632, -1.474695708), 200).iterationCount)
        assertEquals(16, mandelbrotSet.calculatePoint(Complex(-0.772901774, -0.173739807), 200).iterationCount)
        assertEquals(199, mandelbrotSet.calculatePoint(Complex(-0.750124807, -0.015603565), 200).iterationCount)
        assertEquals(200, mandelbrotSet.calculatePoint(Complex(-0.750137202, -0.01554945), 200).iterationCount)
        assertEquals(200, mandelbrotSet.calculatePoint(Complex(0.0, 0.0), 200).iterationCount)
    }
}