import mandelbrot.Complex
import org.junit.jupiter.api.Test
import kotlin.test.assertEquals

class ComplexTests {
    private val absoluteTolerance = 1.0E-14

    @Test
    fun testComplexPlus() {
        val complex1 = Complex(1.5, -3.0)
        val complex2 = Complex(-1.2, 4.2)
        val sum = complex1.plus(complex2)

        assertEquals(0.3, sum.real, absoluteTolerance)
        assertEquals(1.2, sum.imaginary, absoluteTolerance)
    }

    @Test
    fun testComplexSquare() {
        val complex = Complex(1.2, -3.2)
        val square = complex.square()

        assertEquals(-8.8, square.real, absoluteTolerance)
        assertEquals(-7.68, square.imaginary, absoluteTolerance)
    }
}