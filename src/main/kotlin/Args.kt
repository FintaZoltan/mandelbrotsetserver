import com.xenomachina.argparser.ArgParser

class Args(parser: ArgParser) {
    val width by parser.storing(
        "-w", "--width",
        help = "Width of pixel matrix"
    ) { toInt() }

    val height by parser.storing(
        "-h", "--height",
        help = "Height of pixel matrix"
    ) { toInt() }

    val rowSize by parser.storing(
        "-rs", "--rowSize",
        help = "Length of the request matrix row"
    ) { toInt() }

    val columnSize by parser.storing(
        "-cs", "--columnSize",
        help = "Length of the request matrix column"
    ) { toInt() }
}