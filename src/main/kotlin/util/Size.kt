package util

data class Size(val width: Int, val height: Int)