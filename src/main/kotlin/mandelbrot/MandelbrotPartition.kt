package mandelbrot

import util.Size
import java.awt.image.BufferedImage

class MandelbrotPartition(
    from: Complex,
    to: Complex,
    private val size: Size
) {
    private val mandelbrotSet = MandelbrotSet(from, to, size)
    private val valueMatrix = MutableList(size.height) { _ -> MutableList(size.width) { _ -> Complex(0.0, 0.0) } }
    private val iterationMatrix = MutableList(size.height) { _ -> MutableList(size.width) { _ -> 0 } }
    var calculatedMaxIteration: Int = 0
        private set
    private val image = BufferedImage(size.width, size.height, BufferedImage.TYPE_INT_RGB)

    fun makeImage(maxIteration: Int): BufferedImage {
        if (maxIteration < calculatedMaxIteration) return image

        for (y in 0..<size.height) {
            for (x in 0..<size.width) {
                val mandelbrotValue = mandelbrotSet.calculatePoint(x, y,
                    maxIteration = maxIteration,
                    fromPoint = valueMatrix[y][x],
                    fromIteration = iterationMatrix[y][x])
                valueMatrix[y][x] = mandelbrotValue.point
                iterationMatrix[y][x] = mandelbrotValue.iterationCount
                image.setRGB(x, y, mandelbrotValue.color)
            }
        }

        calculatedMaxIteration = maxIteration

        return image
    }
}