package mandelbrot

import util.Size
import java.awt.image.BufferedImage

class MandelbrotSet(
    private val from: Complex,
    private val to: Complex,
    private val imageSize: Size
) {
    fun mapPixelToComplex(x: Int, y: Int): Complex {
        return Complex(
            real = x.toDouble() * (to.real - from.real) / imageSize.width + from.real,
            imaginary = y.toDouble() * (to.imaginary - from.imaginary) / imageSize.height + from.imaginary
        )
    }

    fun calculatePoint(x: Int, y: Int, maxIteration: Int, fromPoint: Complex? = null, fromIteration: Int? = null): MandelbrotValue =
        calculatePoint(mapPixelToComplex(x, y), maxIteration, fromPoint, fromIteration)

    fun calculatePoint(c: Complex, maxIteration: Int, fromPoint: Complex? = null, fromIteration: Int? = null): MandelbrotValue {
        var point = fromPoint ?: c

        var iterationCount = fromIteration ?: 0
        while (iterationCount < maxIteration && point.real * point.real + point.imaginary * point.imaginary <= 4.0) {
            point = point.square() + c

            iterationCount++
        }

        return MandelbrotValue(
            iterationCount,
            point,
            maxIteration,
            if (iterationCount == maxIteration) 0
            else ((iterationCount * 19 % 256) shl 16) + (((iterationCount * 23) % 256) shl 8) + ((iterationCount * 41) % 256)
        )
    }

    fun makeImage(maxIteration: Int): BufferedImage {
        val bufferedImage = BufferedImage(imageSize.width, imageSize.height, BufferedImage.TYPE_INT_RGB)

        for (y in 0..<imageSize.height) {
            for (x in 0..<imageSize.width) {
                val mandelbrotValue = calculatePoint(x, y, maxIteration)
                bufferedImage.setRGB(x, y, mandelbrotValue.color)
            }
        }

        return bufferedImage
    }
}

data class MandelbrotValue(
    val iterationCount: Int,
    val point: Complex,
    val maxIteration: Int,
    val color: Int
)