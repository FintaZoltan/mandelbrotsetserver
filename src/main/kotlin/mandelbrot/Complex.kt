package mandelbrot

data class Complex(val real: Double, val imaginary: Double) {
    fun square(): Complex {
        return Complex(real * real - imaginary * imaginary, 2.0 * real * imaginary)
    }

    operator fun plus(rhs: Complex): Complex {
        return Complex(real + rhs.real, imaginary + rhs.imaginary)
    }
}