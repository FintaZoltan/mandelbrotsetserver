import org.slf4j.Logger


enum class LogType {
    One, Two
}

fun Logger.imageRequestSuccessLog(name: String, row: Int, column: Int, iteration: Int, elapsedTime: Long, logType: LogType) =
    this.info(
        when (logType) {
            LogType.One -> "$name - Image - Success - ${elapsedTime}ms - row: $row - column: $column - iteration: $iteration"
            LogType.Two -> "$name: Image - Success - ${elapsedTime}ms; row: $row, column: $column, iteration: $iteration"
        }
    )

fun Logger.badRequestErrorLog(name: String, request: String, message: String, elapsedTime: Long, logType: LogType) =
    this.info(
        when (logType) {
            LogType.One -> "$name - $request - Bad request - ${elapsedTime}ms - $message"
            LogType.Two -> "$name: $request - Bad request - ${elapsedTime}ms; $message"
        }
    )

fun Logger.clearRequestSuccessLog(name: String, row: Int, column: Int, elapsedTime: Long, logType: LogType) =
    this.info(
        when (logType) {
            LogType.One -> "$name - Clear - Success - ${elapsedTime}ms - row: $row - column: $column"
            LogType.Two -> "$name: Clear - Success - ${elapsedTime}ms; row: $row, column: $column"
        }
    )

fun Logger.sizeRequestSuccessLog(name: String, elapsedTime: Long, logType: LogType) =
    this.info(
        when (logType) {
            LogType.One -> "$name - Size - Success - ${elapsedTime}ms"
            LogType.Two -> "$name: Size - Success - ${elapsedTime}ms"
        }
    )

fun Logger.partitionCalculatedIteration(name: String, row: Int, column: Int, iteration: Int, logType: LogType) {
    this.info(
        when (logType) {
            LogType.One -> "$name - Calculated iteration - row: $row - column: $column - calculatedIteration: $iteration"
            LogType.Two -> "$name: Calculated iteration; row: $row, column: $column, calculatedIteration: $iteration"
        }
    )
}