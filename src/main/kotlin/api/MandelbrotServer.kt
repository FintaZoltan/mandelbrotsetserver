package api

import LogType
import badRequestErrorLog
import clearRequestSuccessLog
import mandelbrot.Complex
import com.sun.net.httpserver.HttpExchange
import com.sun.net.httpserver.HttpServer
import imageRequestSuccessLog
import org.json.JSONObject
import org.slf4j.LoggerFactory
import partitionCalculatedIteration
import sizeRequestSuccessLog
import util.Size
import java.net.InetSocketAddress
import java.util.Date

interface ImageRequestHandlerDelegate {
    fun handleImageRequest(exchange: HttpExchange?)
}

interface SizeRequestHandlerDelegate {
    fun handleSizeRequest(exchange: HttpExchange?)
}

interface ClearRequestHandlerDelegate {
    fun handleClearRequest(exchange: HttpExchange?)
}

class MandelbrotServer(
    private val name: String,
    private val from: Complex,
    private val to: Complex,
    private val imageSize: Size,
    private val partitionMatrixSize: Size,
    private val logType: LogType,
) : ImageRequestHandlerDelegate, SizeRequestHandlerDelegate, ClearRequestHandlerDelegate {
    private val logger = LoggerFactory.getLogger(MandelbrotServer::class.java)

    private val partitionPointSize = Complex(
        (to.real - from.real) / partitionMatrixSize.width,
        (to.imaginary - from.imaginary) / partitionMatrixSize.height
    )
    private val partitionImageSize = Size(
        imageSize.width / partitionMatrixSize.width,
        imageSize.height / partitionMatrixSize.height
    )
    private val mandelbrotSubServers = List(partitionMatrixSize.height) { row ->
        List(partitionMatrixSize.width) { column ->
            MandelbrotSubServer(
                from = Complex(
                    from.real + partitionPointSize.real * column,
                    from.imaginary + partitionPointSize.imaginary * row
                ),
                to = Complex(
                    from.real + partitionPointSize.real * (column + 1),
                    from.imaginary + partitionPointSize.imaginary * (row + 1)
                ),
                imageSize = partitionImageSize,
            )
        }
    }

    fun start(port: Int) {
        for (row in 0..<partitionMatrixSize.height) {
            for (column in 0..<partitionMatrixSize.width) {
                Thread(mandelbrotSubServers[row][column]).start()
            }
        }

        val server = HttpServer.create(InetSocketAddress(port), 0)
        server.createContext("/image", ImageRequestHandler(this))
        server.createContext("/size", SizeRequestHandler(this))
        server.createContext("/clear", ClearRequestHandler(this))
        server.executor = null
        server.start()

        logger.info("$name started on port $port")
    }

    override fun handleImageRequest(exchange: HttpExchange?) {
        val startTime = Date()

        exchange ?: return
        val rawQuery = runCatching { exchange.requestURI.query }.getOrNull() ?: run {
            exchange.respond(400, "Query missing")
            logger.badRequestErrorLog(name, "Image","Query missing", Date().time - startTime.time, logType)
            return@handleImageRequest
        }
        val queryMap = makeQueryMap(rawQuery)

        val partitionRow = runCatching { queryMap["partitionRow"]?.toInt() }.getOrNull() ?: run {
            exchange.respond(400, "'partitionRow' query parameter is missing or not in integer type")
            logger.badRequestErrorLog(name, "Image", "'partitionRow' query parameter is missing or not in integer type", Date().time - startTime.time, logType)
            return@handleImageRequest
        }

        val partitionColumn = runCatching { queryMap["partitionColumn"]?.toInt() }.getOrNull() ?: run {
            exchange.respond(400, "'partitionColumn' query parameter is missing or not in integer type")
            logger.badRequestErrorLog(name, "Image", "'partitionColumn' query parameter is missing or not in integer type", Date().time - startTime.time, logType)
            return@handleImageRequest
        }

        val iteration = runCatching { queryMap["iteration"]?.toInt() }.getOrNull() ?: run {
            exchange.respond(400, "'iteration' query parameter is missing or not in integer type")
            logger.badRequestErrorLog(name, "Image", "'partitionRow' query parameter is missing or not in integer type", Date().time - startTime.time, logType)
            return@handleImageRequest
        }

        if (partitionRow !in 0..<partitionMatrixSize.height || partitionColumn !in 0..<partitionMatrixSize.width) {
            exchange.respond(400, "'partition' is out of range")
            logger.badRequestErrorLog(name, "Image", "'partition' is out of range", Date().time - startTime.time, logType)
            return
        }

        if (iteration <= 0) {
            exchange.respond(400, "'iteration' is not a positive number")
            logger.badRequestErrorLog(name, "Image", "'iteration' is not a positive number", Date().time - startTime.time, logType)
            return
        }

        val currentIteration = mandelbrotSubServers[partitionRow][partitionColumn].calculatedIteration
        mandelbrotSubServers[partitionRow][partitionColumn].taskQueue.offer(SubServerRequest.Image(exchange, iteration) {
            logger.imageRequestSuccessLog(name, partitionRow, partitionColumn, iteration, Date().time - startTime.time, logType)
            if (iteration > currentIteration) {
                logger.partitionCalculatedIteration(name, partitionRow, partitionColumn, iteration, logType)
            }
        })
    }

    override fun handleSizeRequest(exchange: HttpExchange?) {
        val startTime = Date()

        exchange ?: return

        val response = JSONObject(mapOf(
            "width" to partitionMatrixSize.width,
            "height" to partitionMatrixSize.height
        )).toString()

        exchange.responseHeaders["Content-Type"] = "application/json"
        exchange.sendResponseHeaders(200, response.length.toLong())
        exchange.responseBody.write(response.toByteArray())
        exchange.responseBody.close()

        logger.sizeRequestSuccessLog(name, Date().time - startTime.time, logType)
    }

    override fun handleClearRequest(exchange: HttpExchange?) {
        val startTime = Date()

        exchange ?: return

        val rawQuery = runCatching { exchange.requestURI.query }.getOrNull() ?: run {
            exchange.respond(400, "Query missing")
            logger.badRequestErrorLog(name, "Clear", "Query missing", Date().time - startTime.time, logType)
            return@handleClearRequest
        }
        val queryMap = makeQueryMap(rawQuery)

        val partitionRow = runCatching { queryMap["partitionRow"]?.toInt() }.getOrNull() ?: run {
            exchange.respond(400, "'partitionRow' query parameter is missing or not in integer type")
            logger.badRequestErrorLog(name, "Clear", "'partitionRow' query parameter is missing or not in integer type", Date().time - startTime.time, logType)
            return@handleClearRequest
        }

        val partitionColumn = runCatching { queryMap["partitionColumn"]?.toInt() }.getOrNull() ?: run {
            exchange.respond(400, "'partitionColumn' query parameter is missing or not in integer type")
            logger.badRequestErrorLog(name, "Clear", "'partitionColumn' query parameter is missing or not in integer type", Date().time - startTime.time, logType)
            return@handleClearRequest
        }

        if (partitionRow !in 0..<partitionMatrixSize.height || partitionColumn !in 0..<partitionMatrixSize.width) {
            exchange.respond(400, "'partition' is out of range")
            logger.badRequestErrorLog(name, "Clear", "'partition' is out of range", Date().time - startTime.time, logType)
            return
        }

        val calculatedIteration = mandelbrotSubServers[partitionRow][partitionColumn].calculatedIteration

        mandelbrotSubServers[partitionRow][partitionColumn].taskQueue.offer(SubServerRequest.Clear(exchange) {
            if (calculatedIteration != 0) {
                logger.partitionCalculatedIteration(name, partitionRow, partitionColumn, 0, logType)
            }
            logger.clearRequestSuccessLog(name, partitionRow, partitionColumn, Date().time - startTime.time, logType)
        })
    }
}