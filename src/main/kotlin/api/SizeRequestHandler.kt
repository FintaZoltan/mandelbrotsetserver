package api

import com.sun.net.httpserver.HttpExchange
import com.sun.net.httpserver.HttpHandler

class SizeRequestHandler(
    private val sizeRequestHandlerDelegate: SizeRequestHandlerDelegate
) : HttpHandler {
    override fun handle(exchange: HttpExchange?) {
        sizeRequestHandlerDelegate.handleSizeRequest(exchange)
    }
}