package api

import mandelbrot.Complex
import mandelbrot.MandelbrotPartition
import com.sun.net.httpserver.HttpExchange
import util.Size
import java.io.ByteArrayOutputStream
import java.util.concurrent.ArrayBlockingQueue
import java.util.concurrent.BlockingQueue
import javax.imageio.ImageIO

class MandelbrotSubServer(
    private val from: Complex,
    private val to: Complex,
    private val imageSize: Size,
): Runnable {
    val taskQueue: BlockingQueue<SubServerRequest> = ArrayBlockingQueue(10, true)

    private var partition: MandelbrotPartition? = null

    private fun handleImageRequest(exchange: HttpExchange, maxIteration: Int) {
        partition ?: run {
            partition = MandelbrotPartition(from, to, imageSize)
        }

        val image = partition?.makeImage(maxIteration)
        val imageStream = ByteArrayOutputStream()
        ImageIO.write(image, "png", imageStream)

        exchange.responseHeaders["Content-Type"] = "image/png"
        exchange.sendResponseHeaders(200, imageStream.size().toLong())
        exchange.responseBody.write(imageStream.toByteArray())
        exchange.responseBody.close()
    }

    private fun handleClearRequest(exchange: HttpExchange) {
        partition = null
        System.gc()
        exchange.sendResponseHeaders(200, 0)
        exchange.responseBody.close()
    }

    val calculatedIteration: Int
        get() = partition?.calculatedMaxIteration ?: 0

    override fun run() {
        while (true) {
            when (val task = taskQueue.take()) {
                is SubServerRequest.Clear -> {
                    handleClearRequest(task.exchange)
                    task.done()
                }
                is SubServerRequest.Image -> {
                    handleImageRequest(task.exchange, task.maxIteration)
                    task.done()
                }
            }
        }
    }
}

sealed class SubServerRequest {
    data class Clear(val exchange: HttpExchange, val done: () -> Unit = {}) : SubServerRequest()
    data class Image(val exchange: HttpExchange, val maxIteration: Int, val done: () -> Unit = {}) : SubServerRequest()
}
