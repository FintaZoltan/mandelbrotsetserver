package api

import com.sun.net.httpserver.HttpExchange

fun makeQueryMap(query: String): Map<String, String> = query
    .split("&")
    .associate {
        val (key, value) = it.split("=")
        key to value
    }
    .toMap()

fun HttpExchange.respond(rCode: Int, message: String) {
    sendResponseHeaders(rCode, message.length.toLong())
    responseBody.write(message.toByteArray())
    responseBody.close()
}
