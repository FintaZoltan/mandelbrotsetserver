package api

import com.sun.net.httpserver.HttpExchange
import com.sun.net.httpserver.HttpHandler

class ClearRequestHandler(
    private val clearRequestHandlerDelegate: ClearRequestHandlerDelegate
) : HttpHandler {
    override fun handle(exchange: HttpExchange?) {
        clearRequestHandlerDelegate.handleClearRequest(exchange)
    }
}