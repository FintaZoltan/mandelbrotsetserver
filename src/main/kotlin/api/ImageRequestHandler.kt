package api

import com.sun.net.httpserver.HttpExchange
import com.sun.net.httpserver.HttpHandler

class ImageRequestHandler(
    private val imageRequestHandlerDelegate: ImageRequestHandlerDelegate
) : HttpHandler {
    override fun handle(exchange: HttpExchange?) {
        imageRequestHandlerDelegate.handleImageRequest(exchange)
    }

}