import api.MandelbrotServer
import mandelbrot.Complex
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import util.Size
import kotlin.jvm.optionals.getOrElse

val logger: Logger = LoggerFactory.getLogger(MandelbrotServer::class.java)

val requiredParameters = listOf("name", "width", "height", "rowSize", "columnSize", "port", "logType")
fun allRequiredParameters(argsMap: Map<String, String>): Boolean = requiredParameters.all { argsMap.containsKey("-$it") }

fun main(args: Array<String>) {
    if (args.size != requiredParameters.size * 2) {
        logger.error("Number of args are incorrect: ${args.reduce { a, b -> "$a $b" }}")
        return
    }

    val argMap = args.toList().chunked(2).associate { it[0] to it[1] }
    if (!allRequiredParameters(argMap)) {
        logger.error("Missing parameters: ${args.reduce { a, b -> "$a=$b " }}")
        return
    }

    try {
        val name = argMap["-name"]!!
        val width = argMap["-width"]!!.toInt()
        val height = argMap["-height"]!!.toInt()
        val rowSize = argMap["-rowSize"]!!.toInt()
        val columnSize = argMap["-columnSize"]!!.toInt()
        val port = argMap["-port"]!!.toInt()
        val logType = if (argMap["-logType"] == "1") {
            LogType.One
        } else if (argMap["-logType"] == "2") {
            LogType.Two
        } else {
            throw NumberFormatException()
        }

        logger.info("$name will start with - width: $width - height: $height - rowSize: $rowSize - columnSize: $columnSize")

        val mandelbrotServer = MandelbrotServer(
            name = name,
            from = Complex(-2.0, -1.5),
            to = Complex(1.0, 1.5),
            imageSize = Size(width, height),
            partitionMatrixSize = Size(rowSize, columnSize),
            logType = logType
        )

        mandelbrotServer.start(port)
    } catch (_: NumberFormatException) {
        logger.error("Parameters in wrong type: ${args.reduce { a, b -> "$a $b," }}")
        return
    }
}